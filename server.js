var express = require('express');
var app = express();
var path = require('path');

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
//app.listen( 8558, "192.168.1.7");
app.listen(process.env.OPENSHIFT_NODEJS_PORT ,process.env.OPENSHIFT_NODEJS_IP);

app.get('/', function(req, res) {
	res.render('index');
});